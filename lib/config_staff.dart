import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vertigo/services/api_services.dart';
import 'package:flutter_vertigo/widgets/forms/form_add_user.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class ConfigStaff extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ConfigStaffState();
  }
}

class ConfigStaffState extends State<ConfigStaff> {
  var userList = List();
  bool isLoading = true;

  Future<String> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('token');
    return token;
  }

  void getUsers() {
    getToken().then((token) {
      ApiService.getAllUser(token).then((response) async {
        if (response['status'] == 'OK') {
          var users = response['users'];
          userList.clear();
          for (var user in users) {
            getRole(user);
            // var role = await test(user['id_role']);
            // setState(() {
            //   userList.add({
            //     'user': user,
            //     'role': role,
            //   });
            // });
          }
          setState(() {
            isLoading = false;
          });
        }
      });
    });
  }

  Future<String> test(role) async {
    var token = await getToken();
    var response = await ApiService.getRole(role, token);
    if (response['roles'] != null) {
      return response['roles']['name'];
    } else {
      return 'SuperAdmin';
    }
  }

  void getRole(user) {
    getToken().then((token) {
      ApiService.getRole(user['id_role'], token).then((response) {
        if (response['status'] == 'OK' && response['roles'] != null) {
          setState(() {
            userList.add({
              'user': user,
              'role': response['roles']['name'],
            });
          });
        }
      });
    });
  }

  void deleteUser(id) {
    getToken().then((token) {
      ApiService.deleteUser(id, token).then((response) {
        print(response);
        getUsers();
        Toast.show(
          'Deleted successfully',
          context,
          gravity: Toast.BOTTOM,
          duration: Toast.LENGTH_LONG,
        );
      });
    });
  }

  @override
  void initState() {
    super.initState();
    // test();
    getUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Manage Staff',
          style: TextStyle(
            color: Colors.vertigo,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0.5,
        iconTheme: IconThemeData(color: Colors.vertigo),
      ),
      body: Container(
        child: isLoading
            ? Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: userList.length,
                itemBuilder: (context, index) {
                  return Dismissible(
                    key: ValueKey(userList[index]['user']['id']),
                    direction: DismissDirection.endToStart,
                    confirmDismiss: (e) {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return AlertDialog(
                              title: const Text("Confirm"),
                              content: const Text(
                                  "Are you sure you want to delete this user ?"),
                              actions: <Widget>[
                                FlatButton(
                                    onPressed: () {
                                      setState(() {
                                        isLoading = true;
                                      });
                                      deleteUser(userList[index]['user']['id']);
                                      Navigator.of(context).pop(true);
                                    },
                                    child: const Text("DELETE")),
                                FlatButton(
                                  onPressed: () =>
                                      Navigator.of(context).pop(false),
                                  child: const Text("CANCEL"),
                                ),
                              ],
                            );
                          });
                    },
                    background: Container(
                      padding: EdgeInsets.only(right: 16),
                      color: Colors.red,
                      child: Icon(
                        Icons.delete,
                      ),
                      alignment: Alignment.centerRight,
                    ),
                    child: ListTile(
                      title: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              '${userList[index]['user']['name']}',
                              style: TextStyle(fontSize: 20),
                            ),
                            Text(
                              '${userList[index]['role']}',
                              style:
                                  TextStyle(fontSize: 14, color: Colors.grey),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        var userId = userList[index]['user']['id'];
                        Navigator.push(
                          context,
                          CupertinoPageRoute(
                            builder: (builder) => FormUser(
                              status: userId,
                            ),
                          ),
                        );
                      },
                    ),
                  );
                },
              ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
        ),
        onPressed: () {
          Navigator.push(
            context,
            CupertinoPageRoute(
              builder: (builder) => FormUser(
                status: 'add',
              ),
            ),
          );
        },
      ),
    );
  }
}
