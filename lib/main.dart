import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_vertigo/home.dart';
import 'package:flutter_vertigo/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MyApp());

class Urls {
  static const BASE_API_URL = 'https://vertigoapi.herokuapp.com/api';
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: LoadingPage(),
      routes: {
        '/login': (context) => LoginPage(),
        '/home': (context) => HomePage(),
      },
      theme: ThemeData(primarySwatch: Colors.vertigo),
    );
  }
}

class LoadingPage extends StatefulWidget {
  LoadingPage({Key key}) : super(key: key);

  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  @override
  void initState() {
    super.initState();
    loadPage();
  }

  loadPage() {
    getUserStatus().then((userStatus) {
      if (userStatus == null) {
        Navigator.of(context).pushNamed('/login');
      } else {
        Navigator.of(context).pushNamed('/home');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        child: Center(
      child: CircularProgressIndicator(),
    ));
  }
}

Future<String> getUserStatus() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var userStatus = prefs.getString('user');
  print("==On Load Check ==");
  print(userStatus);
  return userStatus;
}
