import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vertigo/config.dart';
import 'package:flutter_vertigo/equipment_booking_system.dart';
import 'package:flutter_vertigo/maintenance_schedule_system.dart';
import 'package:flutter_vertigo/my_activity.dart';
import 'package:flutter_vertigo/staff_assignment_management.dart';
import 'package:flutter_vertigo/tender_management_system.dart';
import 'package:flutter_vertigo/transport_booking_system.dart';

class HomeCard extends StatelessWidget {
  final title;
  final icon;
  final destinationPage;

  HomeCard(this.title, this.icon, this.destinationPage);

  void goTo(context, destinationPage) {
    var path;
    if (destinationPage == 'sam') {
      path = StaffAssignmentManagement();
    } else if (destinationPage == 'ebs') {
      path = EquipmentBookingSystem();
    } else if (destinationPage == 'tbs') {
      path = TransportBookingSystem();
    } else if (destinationPage == 'mss') {
      path = MaintenanceScheduleSystem();
    } else if (destinationPage == 'tms') {
      path = TenderManagementSystem();
    } else if (destinationPage == 'ma') {
      path = MyActivity();
    } else if (destinationPage == 'conf') {
      path = ConfigPage();
    }

    Navigator.push(
      (context),
      CupertinoPageRoute(builder: (builder) => path),
    );
  }

  @override
  Widget build(BuildContext context) {
    final device = MediaQuery.of(context);
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      child: InkWell(
        borderRadius: BorderRadius.circular(20),
        splashColor: Colors.vertigo,
        onTap: () {
          goTo(context, this.destinationPage);
        },
        child: Container(
          padding: EdgeInsets.all(20),
          // height: (device.size.width / 2) - 60,
          // width: (device.size.width / 2) - 30,
          height: 140,
          width: 160,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Icon(this.icon),
              SizedBox(
                height: 10,
              ),
              Text(
                this.title,
                style: TextStyle(fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
