import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vertigo/services/api_services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class FormUser extends StatefulWidget {
  final status;

  FormUser({this.status});

  @override
  State<StatefulWidget> createState() {
    return FormUserState(status: this.status);
  }
}

class FormUserState extends State<FormUser> {
  final status;

  FormUserState({this.status});

  var _role;
  var _roleList = List();
  var _userId;

  bool isLoading = false;
  bool isEdit = false;

  TextEditingController _nameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmController = TextEditingController();

  @override
  void initState() {
    super.initState();
    getRoles();
    if (status != 'add') {
      getUser(status);
      setState(() {
        isEdit = true;
        _userId = status;
      });
    }
  }

  Future<String> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var token = prefs.getString('token');
    return token;
  }

  void getRoles() {
    getToken().then((token) {
      ApiService.getAllRole(token).then((response) {
        var roles = response['roles'];
        for (var role in roles) {
          setState(() {
            _roleList.add(role);
          });
        }
      });
    });
  }

  void getUser(id) {
    getToken().then((token) {
      ApiService.getUser(id, token).then((response) {
        print(response);
        if (response['status'] == 'OK') {
          var user = response['user'];
          setState(() {
            _nameController.text = user['name'];
            _emailController.text = user['email'];
            _role = user['id_role'];
          });
        }
      });
    });
  }

  void addUser() {
    setState(() {
      isLoading = true;
    });
    var _name = _nameController.text;
    var _email = _emailController.text;
    var _password = _passwordController.text;
    var _confirm = _confirmController.text;

    var postData = {
      'name': _name,
      'email': _email,
      'password': _password,
      'password_confirmation': _confirm,
      'id_role': _role,
    };

    getToken().then((token) {
      ApiService.addUser(postData, token).then((response) {
        setState(() {
          isLoading = false;
        });
        if (response['status'] == 'OK') {
          Navigator.pop(context);
          Toast.show(
            'Successfully add user',
            context,
            gravity: Toast.BOTTOM,
            duration: Toast.LENGTH_LONG,
          );
        } else if (response['status'] == 'error') {
          if (response['data']['errors']['email'][0] ==
              'The email has already been taken.') {
            Toast.show('Email already been taken', context,
                gravity: Toast.BOTTOM, duration: Toast.LENGTH_LONG);
          }
          if (response['data']['errors']['password'][0] ==
              'The password confirmation does not match.') {
            Toast.show('Password not match', context,
                gravity: Toast.BOTTOM, duration: Toast.LENGTH_LONG);
          }
        }
      });
    });
  }

  void updateUser() {
    setState(() {
      isLoading = true;
    });
    var _name = _nameController.text;
    var _email = _emailController.text;
    var _password = _passwordController.text;
    var _confirm = _confirmController.text;

    var postData = {
      'name': _name,
      'email': _email,
      'password': _password,
      'password_confirmation': _confirm,
      'id_role': _role,
    };

    getToken().then((token) {
      ApiService.updateUser(postData, _userId, token).then((response) {
        setState(() {
          isLoading = false;
        });
        if (response['status'] == 'OK') {
          Navigator.pop(context);
          Toast.show(
            'Successfully update user',
            context,
            gravity: Toast.BOTTOM,
            duration: Toast.LENGTH_LONG,
          );
        } else if (response['status'] == 'error') {
          if (response['data']['errors']['email'][0] ==
              'The email has already been taken.') {
            Toast.show('Email already been taken', context,
                gravity: Toast.BOTTOM, duration: Toast.LENGTH_LONG);
          }
          if (response['data']['errors']['password'][0] ==
              'The password confirmation does not match.') {
            Toast.show('Password not match', context,
                gravity: Toast.BOTTOM, duration: Toast.LENGTH_LONG);
          }
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          isEdit ? 'Edit Staff' : 'Add New Staff',
          style: TextStyle(
            color: Colors.vertigo,
          ),
        ),
        elevation: 0.5,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.vertigo,
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(20),
        width: double.infinity,
        child: Column(
          children: <Widget>[
            TextField(
              controller: _nameController,
              decoration: InputDecoration(hintText: 'Name'),
            ),
            SizedBox(
              height: 20,
            ),
            TextField(
              controller: _emailController,
              decoration: InputDecoration(hintText: 'Email Address'),
            ),
            SizedBox(
              height: 20,
            ),
            TextField(
              controller: _passwordController,
              decoration: InputDecoration(hintText: 'Password'),
              obscureText: true,
            ),
            SizedBox(
              height: 20,
            ),
            TextField(
              controller: _confirmController,
              decoration: InputDecoration(hintText: 'Confirm Password'),
              obscureText: true,
            ),
            SizedBox(
              height: 20,
            ),
            DropdownButton<String>(
              isExpanded: true,
              hint: Text('Choose User Role'),
              value: _role,
              items: _roleList.map((value) {
                return DropdownMenuItem<String>(
                  value: value['id'],
                  child: Text(value['name']),
                );
              }).toList(),
              onChanged: (value) {
                setState(() {
                  _role = value;
                });
              },
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  isLoading
                      ? CircularProgressIndicator()
                      : Wrap(
                          children: <Widget>[
                            RaisedButton.icon(
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(14),
                                side:
                                    BorderSide(color: Colors.vertigo, width: 2),
                              ),
                              color: Colors.white,
                              icon: Icon(
                                Icons.arrow_back,
                              ),
                              label: Text(
                                'Back',
                              ),
                              onPressed: () {},
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            isEdit
                                ? RaisedButton.icon(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(14),
                                    ),
                                    color: Colors.vertigo,
                                    icon: Icon(
                                      Icons.refresh,
                                      color: Colors.white,
                                    ),
                                    label: Text(
                                      'Update',
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                    onPressed: () {
                                      updateUser();
                                    },
                                  )
                                : RaisedButton.icon(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(14),
                                    ),
                                    color: Colors.vertigo,
                                    icon: Icon(
                                      Icons.check,
                                      color: Colors.white,
                                    ),
                                    label: Text(
                                      'Add',
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                    onPressed: () {
                                      addUser();
                                    },
                                  ),
                          ],
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
