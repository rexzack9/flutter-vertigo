import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AddEquipment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AddEquipmentState();
  }
}

class AddEquipmentState extends State<AddEquipment> {
  var _category;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0.5,
        iconTheme: IconThemeData(color: Colors.vertigo),
        title: Text(
          'Add New Equipment',
          style: TextStyle(
            color: Colors.vertigo,
          ),
        ),
      ),
      body: Container(
        padding: EdgeInsets.all(30),
        width: double.infinity,
        child: Column(
          children: <Widget>[
            addPhoto(),
            SizedBox(
              height: 20,
            ),
            TextField(
              decoration: InputDecoration(hintText: 'Tag Number'),
            ),
            SizedBox(
              height: 10,
            ),
            TextField(
              decoration: InputDecoration(hintText: 'Equipment Name'),
            ),
            SizedBox(
              height: 10,
            ),
            TextField(
              decoration: InputDecoration(hintText: 'Description / Spec'),
            ),
            SizedBox(
              height: 10,
            ),
            DropdownButton<String>(
              isExpanded: true,
              hint: Text('Select equipment category'),
              value: _category,
              items: <String>[
                'Category A',
                'Category B',
                'Category C',
                'Category D'
              ].map((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value),
                );
              }).toList(),
              onChanged: (value) {
                setState(() {
                  _category = value;
                });
              },
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  RaisedButton.icon(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(14),
                      side: BorderSide(color: Colors.vertigo, width: 2),
                    ),
                    color: Colors.white,
                    icon: Icon(
                      Icons.arrow_back,
                    ),
                    label: Text(
                      'Back',
                    ),
                    onPressed: () {
                      print('add');
                    },
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  RaisedButton.icon(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(14),
                    ),
                    color: Colors.vertigo,
                    icon: Icon(
                      Icons.check,
                      color: Colors.white,
                    ),
                    label: Text(
                      'Add',
                      style: TextStyle(
                        color: Colors.white,
                      ),
                    ),
                    onPressed: () {
                      print('add');
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget addPhoto() {
  return InkWell(
    borderRadius: BorderRadius.circular(20),
    child: Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 20,
          )
        ],
        borderRadius: BorderRadius.circular(20),
      ),
      height: 150,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.add_circle,
            color: Colors.grey,
            size: 40,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Upload Photo',
            style: TextStyle(
              color: Colors.grey,
            ),
          ),
        ],
      ),
    ),
    onTap: () {
      print('add picture');
    },
  );
}
