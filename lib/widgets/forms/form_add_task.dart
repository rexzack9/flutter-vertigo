import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AddTask extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AddTaskState();
  }
}

class AddTaskState extends State<AddTask> {
  var user1;
  var staffCount = ['assign_staff[0]'];
  var allUsers = [
    'ahmad saifullah bin abdul hamid',
    'saiful',
    'epul',
    'dadada'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text(
          'Add New Task',
          style: TextStyle(
            color: Colors.vertigo,
          ),
        ),
        backgroundColor: Colors.white,
        elevation: 0.5,
        iconTheme: IconThemeData(
          color: Colors.vertigo,
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Flexible(
                    child: TextField(
                      decoration: InputDecoration(labelText: 'Start Date'),
                    ),
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  Flexible(
                    child: TextField(
                      decoration: InputDecoration(labelText: 'Start Time'),
                    ),
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  Flexible(
                    child: TextField(
                      decoration: InputDecoration(labelText: 'End Date'),
                    ),
                  ),
                  SizedBox(
                    width: 30,
                  ),
                  Flexible(
                    child: TextField(
                      decoration: InputDecoration(labelText: 'End Time'),
                    ),
                  ),
                ],
              ),
              TextField(
                decoration: InputDecoration(labelText: 'Job Number'),
              ),
              TextField(
                decoration: InputDecoration(labelText: 'Job Title'),
              ),
              TextField(
                decoration: InputDecoration(labelText: 'Job Description'),
                keyboardType: TextInputType.multiline,
                maxLines: null,
              ),
              SizedBox(
                height: 20,
              ),
              for (var count in staffCount)
                Container(
                  padding: EdgeInsets.only(top: 20),
                  child: Row(
                    children: <Widget>[
                      Flexible(
                        child: Container(
                          padding: EdgeInsets.only(left: 10, right: 10),
                          decoration: ShapeDecoration(
                            shape: RoundedRectangleBorder(
                              side: BorderSide(
                                width: 1.0,
                              ),
                            ),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: DropdownButton<String>(
                              isExpanded: true,
                              hint: Text('Assign Staff'),
                              value: user1,
                              items: allUsers.map((value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  user1 = value;
                                });
                              },
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        height: 50,
                        width: 50,
                        color: Colors.grey,
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            child: Icon(
                              Icons.add,
                              color: Colors.white,
                            ),
                            onTap: () {
                              print('add staff');
                              setState(() {
                                staffCount.add('assign_staff');
                              });
                            },
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
