import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TransportBookingSystem extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TransportBookingSystemState();
  }
}

class TransportBookingSystemState extends State<TransportBookingSystem> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Transport Booking',
          style: TextStyle(color: Colors.vertigo),
        ),
        backgroundColor: Colors.white,
        elevation: 0.5,
        iconTheme: IconThemeData(color: Colors.vertigo),
      ),
      body: Container(),
    );
  }
}
