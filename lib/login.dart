import 'dart:convert';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vertigo/home.dart';
import 'package:flutter_vertigo/services/api_services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginPageState();
  }
}

class LoginPageState extends State<LoginPage> {
  TextEditingController _usernameController = new TextEditingController();
  TextEditingController _passwordController = new TextEditingController();

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.vertigo,
      body: Container(
        padding: EdgeInsets.all(20),
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(20),
              width: double.infinity,
              color: Colors.transparent,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    'assets/images/logo.png',
                    height: 180,
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(16),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        TextField(
                          controller: _usernameController,
                          decoration: InputDecoration(hintText: 'Username'),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        TextField(
                          controller: _passwordController,
                          obscureText: true,
                          decoration: InputDecoration(hintText: 'Password'),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        isLoading
                            ? Container(
                                child: CircularProgressIndicator(),
                              )
                            : Container(
                                padding: EdgeInsets.all(16),
                                child: RaisedButton.icon(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  color: Colors.vertigo,
                                  icon: Icon(
                                    Icons.check,
                                    color: Colors.white,
                                  ),
                                  label: Text(
                                    'login',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  onPressed: () {
                                    login();
                                  },
                                ),
                              ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void login() async {
    var _username = _usernameController.text;
    var _password = _passwordController.text;

    var emailValidate = EmailValidator.validate(_username);

    if (_username == '' || _password == '') {
      Toast.show(
        'Required username and password.',
        context,
        duration: Toast.LENGTH_LONG,
        gravity: Toast.TOP,
      );
    } else {
      if (!emailValidate) {
        Toast.show(
          'Invalid email.',
          context,
          duration: Toast.LENGTH_LONG,
          gravity: Toast.TOP,
        );
      } else {
        setState(() {
          isLoading = true;
        });

        final postData = {
          'email': '$_username',
          'password': '$_password',
        };

        ApiService.login(postData).then((response) {
          var results = response;
          if (results['user'] != null) {
            setPrefs(results['user'], results['access_token']);
            Navigator.pushReplacement(
              (context),
              MaterialPageRoute(
                builder: (builder) => HomePage(),
              ),
            );
          } else {
            setState(() {
              isLoading = false;
            });
            print('xde data');
            Toast.show(
              'Wrong username or password.',
              context,
              duration: Toast.LENGTH_LONG,
              gravity: Toast.TOP,
            );
          }
        });
      }
    }
  }

  void setPrefs(data, token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', '$token');
    prefs.setString('user', json.encode(data));
  }
}
