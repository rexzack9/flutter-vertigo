import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vertigo/user_profile.dart';
import 'package:flutter_vertigo/widgets/homeCard.dart';
import 'package:feather_icons_flutter/feather_icons_flutter.dart';
import 'package:flutter_vertigo/staff_assignment_management.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomePage> {
  // var suitWidth = Media

  void goPage() {
    Navigator.push(
      (context),
      CupertinoPageRoute(
        builder: (builder) => StaffAssignmentManagement(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final device = MediaQuery.of(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.only(top: 50),
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(left: 20, right: 20),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 10,
                      child: RichText(
                        text: TextSpan(
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 30,
                          ),
                          children: [
                            WidgetSpan(
                              child: Container(
                                padding: EdgeInsets.only(right: 5),
                                child: Text(
                                  'Hello',
                                  style: TextStyle(
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                            WidgetSpan(
                              child: Container(
                                child: Text(
                                  'Saiful',
                                  style: TextStyle(
                                      fontSize: 30,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.vertigo),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 2,
                      child: Icon(Icons.notifications),
                    ),
                    Container(
                      width: 50,
                      height: 50,
                      padding: EdgeInsets.all(3.0), // borde width
                      decoration: BoxDecoration(
                        color: Colors.green, // border color
                        shape: BoxShape.circle,
                      ),
                      child: Container(
                        child: InkWell(
                          child: CircleAvatar(
                            backgroundImage: NetworkImage(
                                'https://images.clipartlogo.com/files/istock/previews/1020/102071463-flat-design-avatar-male-character-icon-vector.jpg'),
                            foregroundColor: Colors.white,
                            backgroundColor: Colors.blue,
                          ),
                          onTap: () {
                            print('profile');
                            Navigator.push(
                              context,
                              CupertinoPageRoute(
                                builder: (builder) => UserProfilePage(),
                              ),
                            );
                          },
                        ),
                        width: 50,
                        height: 50,
                        padding: EdgeInsets.all(2.0), // borde width
                        decoration: BoxDecoration(
                          color: Colors.white, // border color
                          shape: BoxShape.circle,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 30,
              ),
              Column(
                children: [
                  Container(
                    width: device.size.width,
                    child: Wrap(
                      alignment: WrapAlignment.center,
                      spacing: 10.0,
                      runSpacing: 4,
                      children: <Widget>[
                        HomeCard('Staff Assignment Management',
                            FeatherIcons.users, 'sam'),
                        HomeCard('Equipment Booking System',
                            FeatherIcons.printer, 'ebs'),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                children: [
                  Container(
                    width: device.size.width,
                    child: Wrap(
                      alignment: WrapAlignment.center,
                      spacing: 10.0,
                      runSpacing: 4,
                      children: <Widget>[
                        HomeCard('Transport Booking System', FeatherIcons.truck,
                            'tbs'),
                        HomeCard('Maintenance Schedule System',
                            FeatherIcons.settings, 'mss'),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                children: [
                  Container(
                    width: device.size.width,
                    child: Wrap(
                      alignment: WrapAlignment.center,
                      spacing: 10.0,
                      runSpacing: 4,
                      children: <Widget>[
                        HomeCard('Tender Management System',
                            FeatherIcons.layers, 'tms'),
                        HomeCard('My Activity', FeatherIcons.calendar, 'ma'),
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Column(
                children: [
                  Container(
                    width: device.size.width,
                    child: Wrap(
                      alignment: WrapAlignment.center,
                      spacing: 10.0,
                      runSpacing: 4,
                      children: <Widget>[
                        HomeCard('Config',
                            FeatherIcons.layers, 'conf'),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
