import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vertigo/config_staff.dart';
import 'package:flutter_vertigo/widgets/forms/form_add_equipment.dart';

class ConfigPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ConfigPageState();
  }
}

class ConfigPageState extends State<ConfigPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Config',
          style: TextStyle(
            color: Colors.vertigo,
          ),
        ),
        elevation: 0.5,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.vertigo,
        ),
      ),
      body: Container(
        width: double.infinity,
        child: ListView(
          children: <Widget>[
            ListTile(
              title: Text('Staff'),
              trailing: Icon(Icons.chevron_right),
              onTap: () {
                Navigator.push(
                  context,
                  CupertinoPageRoute(
                    builder: (builder) => ConfigStaff(),
                  ),
                );
              },
            ),
            ListTile(
              title: Text('Equipment'),
              trailing: Icon(Icons.chevron_right),
              onTap: () {
                Navigator.push(
                  context,
                  CupertinoPageRoute(
                    builder: (builder) => AddEquipment(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
