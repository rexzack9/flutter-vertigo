import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vertigo/services/api_services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MyActivity extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyActivityState();
  }
}

class MyActivityState extends State<MyActivity> {
  // @override
  // void initState() {
  //   super.initState();
  //   getUsers();
  // }

  // Future<String> getToken() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   var token = prefs.getString('token');
  //   return token;
  // }

  // void getUsers() {
  //   getToken().then((token) {
  //     ApiService.getUsers(token).then((response) {
  //       print(response);
  //     });
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0.5,
          iconTheme: IconThemeData(color: Colors.vertigo),
          bottom: TabBar(
            labelColor: Colors.vertigo,
            indicatorColor: Colors.vertigo,
            unselectedLabelColor: Colors.black,
            tabs: [
              Tab(
                child: Text(
                  'Assigned Task',
                  // style: TextStyle(color: Colors.vertigo),
                ),
              ),
              Tab(
                child: Text(
                  'Requested Task',
                  // style: TextStyle(color: Colors.vertigo),
                ),
              ),
            ],
          ),
          title: Text(
            'My Activity',
            style: TextStyle(color: Colors.vertigo),
          ),
        ),
        body: Container(
          padding: EdgeInsets.only(top: 20),
          color: Colors.white,
          child: TabBarView(
            children: [
              AssignedTask(),
              AssignedTask(),
            ],
          ),
        ),
      ),
    );
  }
}

class AssignedTask extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AssignedTaskState();
  }
}

class AssignedTaskState extends State<AssignedTask> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: <Widget>[
        ListTile(
          title: Row(
            children: <Widget>[
              Text(
                '1)',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(
                width: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        '16 Jul, 9.30 am',
                        style: TextStyle(color: Colors.grey, fontSize: 14),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        child: Text(
                          'SMS',
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                        padding: EdgeInsets.only(left: 10, right: 10),
                        decoration: BoxDecoration(
                          color: Colors.orangeAccent,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ],
                  ),
                  Text(
                    'System enhancement',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ],
          ),
          trailing: Container(
            padding: EdgeInsets.all(10),
            width: 90,
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(10),
            ),
            child: InkWell(
              child: Text(
                'Finished',
                style: TextStyle(color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        ListTile(
          title: Row(
            children: <Widget>[
              Text(
                '2)',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(
                width: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        '16 Jul, 9.30 am',
                        style: TextStyle(color: Colors.grey, fontSize: 14),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        child: Text(
                          'SMS',
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                        padding: EdgeInsets.only(left: 10, right: 10),
                        decoration: BoxDecoration(
                          color: Colors.orangeAccent,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ],
                  ),
                  Text(
                    'System enhancement',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ],
          ),
          trailing: Container(
            padding: EdgeInsets.all(10),
            width: 90,
            decoration: BoxDecoration(
              color: Colors.blue,
              borderRadius: BorderRadius.circular(10),
            ),
            child: InkWell(
              child: Text(
                'Finished',
                style: TextStyle(color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        ListTile(
          title: Row(
            children: <Widget>[
              Text(
                '3)',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              SizedBox(
                width: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        '16 Jul, 9.30 am',
                        style: TextStyle(color: Colors.grey, fontSize: 14),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Container(
                        child: Text(
                          'TMS',
                          style: TextStyle(color: Colors.white, fontSize: 14),
                        ),
                        padding: EdgeInsets.only(left: 10, right: 10),
                        decoration: BoxDecoration(
                          color: Colors.orangeAccent,
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                    ],
                  ),
                  Text(
                    'Equipment management',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ],
          ),
          trailing: Container(
            padding: EdgeInsets.all(10),
            width: 90,
            decoration: BoxDecoration(
              color: Colors.vertigo,
              borderRadius: BorderRadius.circular(10),
            ),
            child: InkWell(
              child: Text(
                'Executing',
                style: TextStyle(color: Colors.white),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
