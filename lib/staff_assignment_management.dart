import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_vertigo/widgets/forms/form_add_task.dart';

class StaffAssignmentManagement extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StaffAssignmentManagementState();
  }
}

class StaffAssignmentManagementState extends State<StaffAssignmentManagement> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Staff Assignment',
          style: TextStyle(color: Colors.vertigo),
        ),
        backgroundColor: Colors.white,
        elevation: 0.5,
        iconTheme: IconThemeData(color: Colors.vertigo),
      ),
      body: Container(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
            context,
            CupertinoPageRoute(
              builder: (builder) => AddTask(),
            ),
          );
        },
      ),
    );
  }
}
