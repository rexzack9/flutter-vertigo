import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class MaintenanceScheduleSystem extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MaintenanceScheduleSystemState();
  }
}

class MaintenanceScheduleSystemState extends State<MaintenanceScheduleSystem> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Maintenance Schedule',
          style: TextStyle(color: Colors.vertigo),
        ),
        backgroundColor: Colors.white,
        elevation: 0.5,
        iconTheme: IconThemeData(color: Colors.vertigo),
      ),
      body: Container(),
    );
  }
}
