import 'dart:convert';

import 'package:flutter_vertigo/main.dart';
import 'package:http/http.dart' as http;

class ApiService {
  static Future<dynamic> _get(String url, String token) async {
    try {
      final response = await http.get(
        url,
        headers: {
          'Accept': 'application/json',
          'Authorization': 'Bearer $token',
        },
      );
      if (response.statusCode == 200) {
        return json.decode(response.body);
      } else {
        return null;
      }
    } catch (err) {
      return null;
    }
  }

  static Future<dynamic> getAllUser(String token) async {
    return await _get('${Urls.BASE_API_URL}/user', token);
  }

  static Future<dynamic> getUser(String id, String token) async {
    return await _get('${Urls.BASE_API_URL}/user/$id', token);
  }

  static Future<dynamic> getAllRole(String token) async {
    return await _get('${Urls.BASE_API_URL}/role', token);
  }

  static Future<dynamic> getRole(String role, String token) async {
    return await _get('${Urls.BASE_API_URL}/role/$role', token);
  }

  static Future<dynamic> login(Map<String, dynamic> postData) async {
    final response = await http.post(
      '${Urls.BASE_API_URL}/login',
      headers: {
        'Accept': 'application/json',
      },
      body: postData,
    );

    var status = response.body.contains('error');
    var data = json.decode(response.body);

    if (status) {
      return false;
    } else {
      return data;
    }
  }

  static Future<dynamic> addUser(
      Map<String, dynamic> postData, String token) async {
    final response = await http.post(
      '${Urls.BASE_API_URL}/register',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: postData,
    );

    var status = response.body.contains('error');
    var data = json.decode(response.body);

    if (status) {
      return {
        'status': 'error',
        'data': data,
      };
    } else {
      return data;
    }
  }

  static Future<dynamic> updateUser(
      Map<String, dynamic> postData, String id, String token) async {
    final response = await http.put(
      '${Urls.BASE_API_URL}/user/$id',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
      body: postData,
    );

    var status = response.body.contains('error');
    var data = json.decode(response.body);

    if (status) {
      return {
        'status': 'error',
        'data': data,
      };
    } else {
      return data;
    }
  }

  static Future<dynamic> deleteUser(String id, String token) async {
    final response = await http.delete(
      '${Urls.BASE_API_URL}/user/$id',
      headers: {
        'Accept': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    var status = response.body.contains('error');
    var data = json.decode(response.body);

    if (status) {
      return {
        'status': 'error',
        'data': data,
      };
    } else {
      return data;
    }
  }
}
