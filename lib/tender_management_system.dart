import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TenderManagementSystem extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TenderManagementSystemState();
  }
}

class TenderManagementSystemState extends State<TenderManagementSystem> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Tender Management',
          style: TextStyle(color: Colors.vertigo),
        ),
        backgroundColor: Colors.white,
        elevation: 0.5,
        iconTheme: IconThemeData(color: Colors.vertigo),
      ),
      body: Container(),
    );
  }
}
